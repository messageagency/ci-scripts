var defaults = {
  standard: 'WCAG2AA',
  level: "error",
  chromeLaunchConfig: {
    "args": ["--no-sandbox"]
  }
};

var urls = [
  '${BASE_URL}'
];

function pa11yConfig() {
  console.log('Env: ', process.env.MULTIDEV_SITE_URL);

  for (var i = 0; i < urls.length; i++) {
    urls[i] = urls[i].replace('${BASE_URL}', process.env.MULTIDEV_SITE_URL);
  }

  return {
    defaults: defaults,
    urls: urls
  }
}

module.exports = pa11yConfig();
